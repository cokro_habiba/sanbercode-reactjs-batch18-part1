//soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var letter = "javascript";
var upper = letter.toUpperCase();

console.log(kataPertama, kataKedua, kataKetiga, (upper))

//soal 2
var kataPertama = Number("1");
var kataKedua = Number("2");
var kataKetiga = Number("4");
var kataKeempat = Number("5");

console.log(kataPertama + kataKedua + kataKetiga + kataKeempat)

//soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

//soal 4
var nilai = 60;
if (nilai >= 80) {
    console.log("indeks A")
} else if (nilai >= 70 && nilai < 80) {
    console.log("indeks B")
} else if (nilai >= 60 && nilai < 70) {
    console.log("indeks C")
} else if (nilai >= 50 && nilai < 50) {
    console.log("indeks D")
} else if (nilai < 50) {
    console.log("indeks E")
}

console.log(nilai)

//soal 5

var tanggal = ("15");
var tahun = ("1994");
var bulan = (0);
switch (bulan) {
    case 0:
        console.log("Januari");
        break;
    case 1:
        console.log("Februari");
        break;
    case 2:
        console.log("Maret");
        break;
    case 3:
        console.log("April");
        break;
    case 4:
        console.log("Mei");
        break;
    case 5:
        console.log("Juni");
        break;
    case 6:
        console.log("Juli");
        break;
    case 7:
        console.log("Agustus");
        break;
    case 8:
        console.log("September");
        break;
    case 9:
        console.log("Oktober");
        break;
    case 10:
        console.log("November");
        break;
    case 11:
        console.log("Desember");
        break;
}

console.log(tanggal, 'Januari', tahun);